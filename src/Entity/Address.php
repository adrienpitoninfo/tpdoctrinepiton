<?php
# src/Entity/Address.php

namespace tpdoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(
    name="Adress",
    indexes={
        @ORM\Index(name="search_idA", columns={"idA"}),
        @ORM\Index(name="search_appart", columns={"appart"}),
        @ORM\Index(name="search_street", columns={"street"}),
        @ORM\Index(name="search_zipcode", columns={"zipcode"}),
        @ORM\Index(name="search_city", columns={"city"}),
        @ORM\Index(name="search_country", columns={"country"})
    }
 )
*/
class Address
{
     /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue
    */
    protected $idA;
     /**
    * @ORM\Column(type="string")
    */
    protected $appart;
     /**
    * @ORM\Column(type="string")
    */
    protected $street;
     /**
    * @ORM\Column(type="integer")
    */
    protected $zipcode;
     /**
    * @ORM\Column(type="string")
    */
    protected $city;
     /**
    * @ORM\Column(type="string")
    */
    protected $country;
   /**
    * @ORM\OneToOne(targetEntity=User::class, mappedBy="address")
    */
    protected $user;


    
    /**
     * Get the value of idA
     */ 
    public function getIdA()
    {
        return $this->idA;
    }

    /**
     * Set the value of idA
     *
     * @return  self
     */ 
    public function setIdA($idA)
    {
        $this->idA = $idA;

        return $this;
    }

    /**
     * Get the value of appart
     */ 
    public function getAppart()
    {
        return $this->appart;
    }

    /**
     * Set the value of appart
     *
     * @return  self
     */ 
    public function setAppart($appart)
    {
        $this->appart = $appart;

        return $this;
    }

    /**
     * Get the value of street
     */ 
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set the value of street
     *
     * @return  self
     */ 
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get the value of zipcode
     */ 
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set the value of zipcode
     *
     * @return  self
     */ 
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of country
     */ 
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }


    public function _toString(){
        return sprintf($this->idA, $this->appart, $this->street, $this->zipcode, $this->city, $this->country);
    }

    /**
     * Get the value of user
     */ 
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */ 
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}